### Back-end for Ascension Portal.

### What's inside this project?
REST              | Spring Boot.
Security          | JWT(Token based) and Spring Security.
API Documentation | Swagger.
Database          | H2 for project disclosure as it can run stand-alone, in the real environment Oracle MySQL is used.
Data Persistence  | Java Persistence API
Build             | Maven/Gradle

### Build
Maven: mvn clean install
Gradle: gradle build

### Information
Spring security is enabled by default, none of the REST API will be accessible without bearer token.
Get bearer token by running this command in terminal/bash:
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"username": "demo", "password": "demo" }' 'http://localhost:9119/session'

Use the bearer token as a security header to make the http request:
curl -X GET --header 'Accept: application/json' --header 'Authorization: [BEARER_TOKEN]' 'http://localhost:9119/version'

URL:
Database       |  http://localhost:9119/h2-console        |  Driver:`org.h2.Driver` <br/> JDBC URL:`jdbc:h2:mem:demo` <br/> User Name:`sa`
Swagger        |  http://localhost:9119/swagger-ui.html   
Swagger Spec   |  http://localhost:9119/api-docs 








