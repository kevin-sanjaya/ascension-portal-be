package com.app.model.division;

import lombok.*;
import javax.persistence.*;

@Data
@Entity
@Table(name = "divisions")
public class Division  {

    @Id
    private Integer id;
    private String name;

    public void generateId(int id) {
        this.id = id;
    }
}
